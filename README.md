kleine Anmerkung: Ich habe letztes Semester Testat bekommen

### Tech
Unser Projekt verwendet eine Reihe von Bibliotheken und Technologien:


- MySQL - database management system
- node.js - evented I/O for the backend
- npm - package manager
- "body-parser": "^1.19.0",
- "class-validator": "^0.12.2",
- "dotenv-safe": "^8.2.0",
- "express": "^4.17.1",
- "mysql2": "^2.1.0",
- "reflect-metadata": "^0.1.13",
- "tsc-watch": "^4.2.3",
- "typeorm": "^0.2.24",
- "typescript": "^3.8.3"


**Installation and Configuration**


1. Klonen Sie das Repository FWE-WS20-21-757851-HA1 auf Ihren lokalen Computer
2. Installieren Sie MySQL-Server (download hier:https://dev.mysql.com/downloads/)
3. Starten den MySQL-Server
4. Erstellen Sie eine Datenbank (Datenbank-name : FWE )
5. Führen Sie das Skript aus
```$ cd FWE-WS20-21-757851-HA1
$ npm run typeorm schema:sync
```

6. Installieren die dependencies und devDependencies und starten den Server.
```sh
$ cd FWE-WS20-21-757851-HA1
$ npm install 
$ npm start
```


### How To Use:

#### REST API

#### Task
 - `GET /api/task/(ID)` - get a task by id
 - `GET /api/task` - get all he saved task
 - `POST /api/task` - create a new task by sending a JSON body e.g:
 ````
 {
    "name" : "test name" ,
    "description" : "Test description" 
}
````
 - `PATCH /api/task/(ID)` - updates a task 
 ````
{
    "name" : "update name" ,
    "description" : "update description" 
}
````
 - `DELETE /api/task/(ID)` - deletes a task by id

- `POST /api/taskLabel/label/(labelId)/task/(taskId)` - assign an label to a task:
````
{
}
````
- `DELETE /api/taskLabel/label/(labelId)/task/(taskId)` - deletes an label from a task 


#### Label

 - `GET /api/label/(ID)` - get a label by id
 - `GET /api/label` - get all he saved label
 - `POST /api/label` - create a new label by sending a JSON body e.g:
 ````
{
    "name" : "test name" 
}
````
 - `PATCH /api/label/(ID)` - updates a label 
 ````
{
    "name" : "update name" ,
}
````
- `DELETE /api/label/(ID)` - deletes a label by id


#### tracking

 - `GET /api/tracking/(ID)` - get a tracking by id
 - `GET /api/tracking` - get all he saved tracking
 - `POST /api/tracking` - create a new tracking by sending a JSON body e.g:
 ````
{
    "description" : "description" ,
    "startTime" : 10 ,
    "endTime" : 100
}
````
 - `PATCH /api/tracking/(ID)` - updates a tracking 
 ````
{
    "description" : "update description" ,
    "startTime" : 10 ,
    "endTime" : 100
}
````
- `DELETE /api/tracking/(ID)` - deletes a tracking by id
- `POST /api/tracking/task/:taskId` - create and Add task to Tracking;


### Testing
1. Erstellen Sie eine leere Datenbank mit dem Namen FWE
2. Gehen Sie zum Projektordner und führen Sie ```npm run typeorm schema:sync``` AUS
3. Öffnen Sie Postman und importieren Sie die JSON-Sammlung im Ordner /postman im Projektverzeichnis
4. Klicken Sie auf den **Collection Runner** und führen Sie die importierte Collection aus