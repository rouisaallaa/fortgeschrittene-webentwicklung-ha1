import { Router } from 'express';
import {
	addLabelToTask , 
	deleteLabelFromTask
} from '../controller/taskLabel.controller';

export const taskLabelRouter = Router({ mergeParams: true });


taskLabelRouter.post('/label/:labelId/task/:taskId', addLabelToTask);

taskLabelRouter.delete('/label/:labelId/task/:taskId', deleteLabelFromTask);

