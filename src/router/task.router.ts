import { Router } from 'express';
import {
	getAllTask  , 
	createTask  , 
	getTaskById ,
	deleteTask  ,
	patchTask
} from '../controller/task.controller';

export const taskRouter = Router({ mergeParams: true });

taskRouter.get('/', getAllTask);

taskRouter.post('/',createTask);

taskRouter.get('/:taskId', getTaskById );

taskRouter.delete('/:taskId', deleteTask);

taskRouter.patch('/:taskId', patchTask);
