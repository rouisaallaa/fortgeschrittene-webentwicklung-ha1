import { Router } from 'express';
import {
	getAllLabel  , 
	createLabel  , 
	getLabelById ,
	deleteLabel  ,
	patchLabel	
} from '../controller/label.controller';

export const labelRouter = Router({ mergeParams: true });

labelRouter.get('/', getAllLabel);

labelRouter.post('/',createLabel);

labelRouter.get('/:labelId', getLabelById );

labelRouter.delete('/:labelId', deleteLabel);

labelRouter.patch('/:labelId', patchLabel);

