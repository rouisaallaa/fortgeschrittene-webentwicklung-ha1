import { Router } from 'express';
import {
	getAllTracking  , 
	createTracking  , 
	getTrackingById ,
	deleteTracking  ,
	patchTracking	,
	addTrackingToTask 
} from '../controller/tracking.controller';

export const trackingRouter = Router({ mergeParams: true });

trackingRouter.get('/', getAllTracking);

trackingRouter.post('/',createTracking);

trackingRouter.get('/:trackingId', getTrackingById );


trackingRouter.delete('/:trackingId', deleteTracking);

trackingRouter.patch('/:trackingId', patchTracking);

trackingRouter.post('/task/:taskId', addTrackingToTask);


