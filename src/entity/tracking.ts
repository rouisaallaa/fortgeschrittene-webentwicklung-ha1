import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne } from 'typeorm';

import { Task } from "./task" ;
import { MinLength, Min, Max, IsInt } from "class-validator" ;


@Entity()
export class Tracking {
  @PrimaryGeneratedColumn('uuid')
  id: number;

  @Column({ type: 'text' , nullable: false})
  @MinLength(3, { message : 'description is too short'})
  description: string;

  
  @Column({ type: 'text' , nullable: false})
  @IsInt()
  startTime: number;

  @Column({ type: 'text' , nullable: false})
  @IsInt()
  endTime: number;
  
  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;


 @ManyToOne(type => Task, task => task.tracking , { eager: false, onDelete: 'CASCADE'})
  public task!: Task;

}
