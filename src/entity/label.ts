import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, Unique, OneToMany } from 'typeorm';
import { Task } from "./task" ;
import { TaskLabel } from "./taskLabel" ;
import { MinLength, Min, Max } from "class-validator" ;

@Entity()
export class Label {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'text' , nullable: false})
  @MinLength(3, { message : 'Name is too short'})
  name: string;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  @OneToMany(type => TaskLabel, taskLabel => taskLabel.label)
  public  tasks!: TaskLabel[];
}
