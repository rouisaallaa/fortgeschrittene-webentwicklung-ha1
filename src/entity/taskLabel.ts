import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, ManyToOne, Unique} from 'typeorm';
import { MinLength, Min, Max } from "class-validator" ;

import { Label } from "./label" ;
import { Task } from "./task" ;
@Entity()
export class TaskLabel {

  @PrimaryGeneratedColumn()
  public labelTaskId!: number;

  @Column()
  labelId: number;

  @Column()
  taskId: number;

  @Column()
  @MinLength(3, { message : 'Name is too short'})
  LabelName: string;

  @CreateDateColumn()
  createdAt!: string;

  @UpdateDateColumn()
  updateAt!: string;

  @ManyToOne(type => Label, label => label.tasks , { eager: false, onDelete: 'CASCADE'})
  public label!: Label;

  @ManyToOne(type => Task, task => task.labels , {eager: false, onDelete: 'CASCADE'})
  public task!: Task;

}
