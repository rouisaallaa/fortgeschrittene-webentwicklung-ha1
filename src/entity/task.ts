import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, Unique, OneToMany } from 'typeorm';

import { Tracking } from "./tracking" ;
import { Label } from "./label" ;
import { TaskLabel } from "./taskLabel" ;
import { MinLength, Min, Max } from "class-validator" ;


@Entity()

export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  @MinLength(3, { message : 'Name is too short'})
  name: string;

  @Column({ type: 'text' , nullable: true})
  @MinLength(3, { message : 'description is too short'})
  description : string;

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: string;

  @OneToMany(type => Tracking, tracking => tracking.task)
  public tracking!: Tracking[];

  @OneToMany(type => TaskLabel, taskLabel => taskLabel.task)
  public labels!: TaskLabel[];


}
