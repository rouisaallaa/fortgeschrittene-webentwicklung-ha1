import { getRepository } from 'typeorm';
import { Label } from '../entity/label';
import { Request, Response } from 'express';
import { validate} from "class-validator";



export const getAllLabel = async (_: Request, res: Response) => {
  const labelRepository = await getRepository(Label);
  const label = await labelRepository.find({ relations: ["tasks" ] });
  res.send({
    data: label,
  });
};

export const createLabel = async (req: Request, res: Response) => {
  const { name } = req.body;

  const label = new Label();
  label.name = name;

  const errors = await validate(label ,{ skipMissingProperties: true } );
 
  const labelRepository = await getRepository(Label);
  const createdLabel = await labelRepository.save(label);
  if(errors.length > 0){ 
    res.status(404).send({
      status: 'error',
    });
  }else{
    res.send({
      data: createdLabel
    });
  }
  
};

export const getLabelById = async (req: Request, res: Response) => {
  const labelId = req.params.labelId;
  const labelRepository = await getRepository(Label);

  try {
    const label = await labelRepository.findOneOrFail(labelId , { relations: ["tasks"  ] });
    res.send({
      data: label,
    });
  } catch (e) {
    res.status(404).send({
      status: 'not_found',
    });
  }
};

export const deleteLabel = async (req: Request, res: Response) => {
  const labelId = req.params.labelId;
  const labelRepository = await getRepository(Label);

  try {
    const label = await labelRepository.findOneOrFail(labelId);
    await labelRepository.remove(label);
    res.send({});
  } catch (e) {
    res.status(404).send({
      status: 'not_found',
    });
  }
};

export const patchLabel = async (req: Request, res: Response) => {
  const labelId = req.params.labelId;
  const { name } = req.body;
  const labelRepository = await getRepository(Label);

  try {
    let label = await labelRepository.findOneOrFail(labelId);
    label.name = name;

    label = await labelRepository.save(label);

    res.send({
      data: label
    });
  } catch (e) {
    res.status(404).send({
      status: 'not_found',
    });
  }
};


