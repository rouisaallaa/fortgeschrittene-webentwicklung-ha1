import { getRepository } from 'typeorm';
import { Tracking } from '../entity/tracking';
import { Request, Response } from 'express';
import { validate} from "class-validator";
import { Task } from '../entity/task';

export const getAllTracking = async (_: Request, res: Response) => {
  const trackingRepository = await getRepository(Tracking);
  const tracking = await trackingRepository.find({ relations: ["task"] } );
  res.send({
    data: tracking,
  });
};

export const createTracking = async (req: Request, res: Response) => {
  const { description, startTime, endTime } = req.body;

  const tracking = new Tracking();
  tracking.description = description;
  tracking.startTime = startTime;
  tracking.endTime = endTime;
  
  const trackingRepository = await getRepository(Tracking);
  const createdTracking = await trackingRepository.save(tracking);
  const errors = await validate(tracking ,{ skipMissingProperties: true } );
  if(errors.length > 0){ 
    res.status(404).send({
      status: 'error',
    });
  }else{
    res.send({
      data: createdTracking
    });
  }
};

export const getTrackingById = async (req: Request, res: Response) => {
  const trackingId = req.params.trackingId;
  const trackingRepository = await getRepository(Tracking);

  try {
    const tracking = await trackingRepository.findOneOrFail(trackingId, { relations: ["task"] } );
    res.send({
      data: tracking,
    });
  } catch (e) {
    res.status(404).send({
      status: 'not_found',
    });
  }
};

export const deleteTracking = async (req: Request, res: Response) => {
  const trackingId = req.params.trackingId;
  const trackingRepository = await getRepository(Tracking);

  try {
    const tracking = await trackingRepository.findOneOrFail(trackingId);
    await trackingRepository.remove(tracking);
    res.send({});
  } catch (e) {
    res.status(404).send({
      status: 'not_found',
    });
  }
};

export const patchTracking = async (req: Request, res: Response) => {
  const trackingId = req.params.trackingId;
  const { description, startTime, endTime } = req.body;

  const trackingRepository = await getRepository(Tracking);

  try {
    let tracking = await trackingRepository.findOneOrFail(trackingId);
    tracking.description = description;
    tracking.startTime = startTime;
    tracking.endTime = endTime;

    tracking = await trackingRepository.save(tracking);

    res.send({
      data: tracking
    });
  } catch (e) {
    res.status(404).send({
      status: 'not_found',
    });
  }
};


export const addTrackingToTask = async (req: Request, res: Response) => {
  const { description, startTime, endTime } = req.body;
  const taskId = req.params.taskId;

  const taskRepository = await getRepository(Task);
  const task = await taskRepository.findOneOrFail(taskId);
  const tracking = new Tracking();
  tracking.description = description;
  tracking.startTime = startTime;
  tracking.endTime = endTime;
  tracking.task = task;
  const trackingRepository = await getRepository(Tracking);
  const createdTracking = await trackingRepository.save(tracking);
  const errors = await validate(tracking ,{ skipMissingProperties: true } );
  if(errors.length > 0){ 
    res.status(404).send({
      status: 'error',
    });
  }else{
    res.send({
      data: createdTracking
    });
  }
};