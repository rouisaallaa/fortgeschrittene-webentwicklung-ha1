import { getRepository } from 'typeorm';
import {getConnection} from "typeorm";

import { TaskLabel } from '../entity/taskLabel';
import { Label } from '../entity/label';
import { Task } from '../entity/task';


import { Request, Response } from 'express';

export const addLabelToTask = async (req: Request, res: Response) => {

    try {
      const labelId = req.params.labelId;
      const taskId = req.params.taskId;

      const labelRepository = await getRepository(Label);
      const label = await labelRepository.findOneOrFail(labelId);

      const taskRepository = await getRepository(Task);
      const task = await taskRepository.findOneOrFail(taskId);

      const taskLabel = new TaskLabel() ;

      taskLabel.label = label ;
      taskLabel.labelId = label.id ;
      taskLabel.task = task ;
      taskLabel.taskId = task.id ;
      taskLabel.LabelName = label.name; 

      const taskLabelRepository = await getRepository(TaskLabel);
      const taskLabell = await taskLabelRepository.save(taskLabel);

      res.send({
          data: taskLabell,
        });
      } catch (e) {
        res.status(404).send({
          status: 'not_found',
        });
      }
};


export const deleteLabelFromTask = async (req: Request, res: Response) => {

    try {
      const labelId = req.params.labelId;
      const taskId = req.params.taskId;

      const labelRepository = await getRepository(Label);
      const taskRepository = await getRepository(Task);
      const taskLabelRepository = await getRepository(TaskLabel);

      await labelRepository.findOneOrFail(labelId);
      await taskRepository.findOneOrFail(taskId);
      await getConnection()
      .createQueryBuilder()
      .delete()
      .from(TaskLabel)
      .where("labelId = :id AND taskId = :idd", { id: labelId , idd : taskId })
      .execute();

      res.send({
          data: `label id = ${labelId} is deleted from task id = ${taskId}`
        });
      } catch (e) {
        res.status(404).send({
          status: 'not_found',
        });
      }
};


