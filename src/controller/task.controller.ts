import { getRepository } from 'typeorm';
import { Task } from '../entity/task';
import { Request, Response } from 'express';
import { validate} from "class-validator";

export const getAllTask = async (_: Request, res: Response) => {
  const taskRepository = await getRepository(Task);
  const task = await taskRepository.find({ relations: ["tracking" , "labels"] });
  res.send({
    data: task,
  });
};

export const createTask = async (req: Request, res: Response) => {
  const { name, description } = req.body;

  const task = new Task();
  task.name = name;
  task.description = description;

  const errors = await validate(task ,{ skipMissingProperties: true } );
  
  const taskRepository = await getRepository(Task);
  const createdTask = await taskRepository.save(task);
  if(errors.length > 0){ 
    res.status(404).send({
      status: 'error',
    });
  }else{
    res.send({
      data: createdTask
    });
  }
};

export const getTaskById = async (req: Request, res: Response) => {
  const taskId = req.params.taskId;
  const taskRepository = await getRepository(Task);

  try {
    const task = await taskRepository.findOneOrFail(taskId , { relations: ["tracking" , "labels"] } );
    res.send({
      data: task,
    });
  } catch (e) {
    res.status(404).send({
      status: 'not_found',
    });
  }
};

export const deleteTask = async (req: Request, res: Response) => {
  const taskId = req.params.taskId;
  const taskRepository = await getRepository(Task);

  try {
    const task = await taskRepository.findOneOrFail(taskId);
    await taskRepository.remove(task);
    res.send({});
  } catch (e) {
    res.status(404).send({
      status: 'not_found',
    });
  }
};

export const patchTask = async (req: Request, res: Response) => {
  const taskId = req.params.taskId;
  const { name, description } = req.body;
  const taskRepository = await getRepository(Task);

  try {
    let task = await taskRepository.findOneOrFail(taskId);
    task.name = name;
    task.description = description;

    task = await taskRepository.save(task);

    res.send({
      data: task
    });
  } catch (e) {
    res.status(404).send({
      status: 'not_found',
    });
  }
};

export const getAllTaskByLabelId = async (req: Request, res: Response) => {


};
